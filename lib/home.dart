import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _score = 0;
  var _randomText = "Hey man ! ";
  var _start = true;
  _buttonOnPressed() {
    setState(() {
      _score++;
    });
  }

  _starting() {
    setState(() {
      _start = !_start;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue[900],
          title: Title(color: Colors.black, child: Text("Tardos Clicker")),
        ),
        body: SafeArea(
          child: Center(
              child: Column(
            children: [
              _start ? Text("En attente") : Text("Votre score : $_score"),
              _start
                  ? ElevatedButton(
                      child: Text("Commencer la partie"), onPressed: _starting)
                  : IconButton(
                      icon: Icon(Icons.add), onPressed: _buttonOnPressed),
            ],
          )),
        ));
  }
}
